package com.feds.store.messages.communicator;

public interface DataMessage<T> {

    void onReceiveData(T t);
}
